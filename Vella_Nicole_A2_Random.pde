// A random integer between 10-100 which will be used to draw X number of dots.
int numDots = round(random(10,100));

// Two arrays of integer values, one to store the X coordinate of the generated points, and one for the Y
int[] dataX;
int[] dataY;

// Four variables for each line to draw, the starting XY coordinates and the ending XY coordinates
int startX;
int startY;
int endX;
int endY;

// A variable to count how many lines we have drawn.
int count = 0;

void setup() {

  // Set the background and size of the canvas
  background(0);
  size(1000, 1000);

  // Use the HSB color mode (Hue, Saturation, Brightness) using ranges of 0-numDots for hue, 0-100 for saturation, and 0-100 for brightness.
  colorMode(HSB, numDots, 100, 100);

  // Initalize the two arrays, each one with a length == the total number of dots.
  dataX = new int[numDots];
  dataY = new int[numDots];

  // A for loop to draw dots in random places and number them.
  for (int i=0; i<numDots; i++) {

    // Generate two random integers between 0 and the width/height of the canvas.
    int randX = round(random(width));
    int randY = round(random(height));

    // Add the X and Y coordinate integers to their respective arrays.
    dataX[i] = randX;
    dataY[i] = randY;

    // Draw circles with no stroke.
    noStroke();
    fill(0, 0, 100);
    circle(randX, randY, 3);

    // Draw text next to each dot, numbering them.
    fill(0, 0, 50);
    textSize(8);
    text((i+1), randX+5, randY+5);
  }
}

void draw() {
  
  // If statement runs once for each numbered dot
  if (count < (numDots-1)) {

    // Set the values/variables for the line starting point
    startX = dataX[count];
    startY = dataY[count];

    // Set the values/variables for the line ending point
    endX = dataX[count+1];
    endY = dataY[count+1];

    // Cycle through the colour wheel, changing with each line drawn.
    strokeWeight(3);
    // By using "count" for the hue of the line, we can go through the entire color
    // spectrum since we have already set the colorMode to HSB and the hue has a range
    // from 0 to numDots
    stroke(count, 100, 100);

    // Connect the dots!
    line(startX, startY, endX, endY);

    // A delay in between each line drawn because why not animate it?
    delay(50);

    // Add 1 to the count.
    count++;
  } else if (count == (numDots-1)) {

    // we've drawn all the lines, let's draw one more and connect it from the last point to the first point 
    startX = dataX[count];
    startY = dataY[count];
    endX = dataX[0];
    endY = dataY[0];

    // Draw the line back to the original point.
    stroke(count, 100, 100);
    line(startX, startY, endX, endY);
  } else {

    // All lines have been drawn. Return nothing/Do nothing.
    return;
  }
}